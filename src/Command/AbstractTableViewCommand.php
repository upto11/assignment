<?php

declare(strict_types=1);

namespace App\Command;

use App\Exam\Exam;
use App\Exam\ExamData;
use App\File\Handler\ExcelFileHandler;
use App\File\Loader;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

abstract class AbstractTableViewCommand extends Command
{
    public const MAX_PER_PAGE = 20;

    /**
     * @var Loader
     */
    protected $fileLoader;

    public function __construct()
    {
        parent::__construct();

        $this->fileLoader = new Loader(new ExcelFileHandler());
    }

    protected function configure(): void
    {
        $this
            ->setDescription($this->getCommandDescription())
            ->addArgument('file', InputArgument::REQUIRED, 'The file to load');
    }

    abstract public function getCommandDescription(): string;

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var string $file */
        $file = $input->getArgument('file');

        $this->fileLoader->load($file);

        $data = new ExamData($this->fileLoader->getData());
        $exam = new Exam($data);

        $page = 0;

        $this->renderPage($output, $page, $exam);

        $helper = $this->getHelper('question');
        $question = new Question('[q] quit [n] next [p] previous [0-9] page: ', 'q');

        $pages = $this->getTotalNrOfPages($exam);

        while ('q' !== ($answer = $helper->ask($input, $output, $question))) {
            switch ($answer) {
                case 'n':
                    ++$page;
                    $page = $page > $pages ? 0 : $page;

                    $this->renderPage($output, $page, $exam);
                    break;
                case 'p':
                    --$page;
                    $page = $page < 0 ? 0 : $page;

                    $this->renderPage($output, $page, $exam);
                    break;
                default:
                    if ((int) $answer) {
                        $page = (int) $answer - 1;
                        $page = $page > $pages ? 0 : $page;
                    }

                    $this->renderPage($output, $page, $exam);
                    break;
            }
        }

        return 0;
    }

    protected function renderPage(OutputInterface $output, int $page, Exam $exam): void
    {
        $table = new Table($output);
        $table->setHeaders($this->getHeaders());
        $table->setFooterTitle(sprintf('Page %d / %d', $page + 1, $this->getTotalNrOfPages($exam) + 1));
        $this->setTableBody($page, $table, $exam);
        $table->render();
    }

    abstract protected function getHeaders(): array;

    abstract protected function getTotalNrOfPages(Exam $exam): float;

    abstract protected function setTableBody(int $page, Table $table, Exam $exam): void;
}
