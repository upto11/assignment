<?php

declare(strict_types=1);

namespace App\Command;

use App\Exam\Exam;
use App\Exam\Student;
use Symfony\Component\Console\Helper\Table;

class ResultsCommand extends AbstractTableViewCommand
{
    protected static $defaultName = 'results';

    public function getCommandDescription(): string
    {
        return 'Show a table with all student results';
    }

    protected function setTableBody(int $page, Table $table, Exam $exam): void
    {
        collect($exam->getStudents())
            ->slice($page * self::MAX_PER_PAGE, self::MAX_PER_PAGE)
            ->each(static function (Student $student) use ($exam, &$table) {
                $table->addRow([
                    $student->getName(),
                    number_format($exam->getGrade($student), 1),
                    ($exam->studentPassed($student) ? '<fg=green>Yes</>' : '<fg=red>No</>'),
                ]);
            });
    }

    protected function getTotalNrOfPages(Exam $exam): float
    {
        return \count($exam->getStudents()) / self::MAX_PER_PAGE;
    }

    protected function getHeaders(): array
    {
        return ['Student', 'Grade', 'Passed'];
    }
}
