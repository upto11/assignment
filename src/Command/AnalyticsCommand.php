<?php

declare(strict_types=1);

namespace App\Command;

use App\Exam\Analytics;
use App\Exam\Exam;
use Symfony\Component\Console\Helper\Table;

class AnalyticsCommand extends AbstractTableViewCommand
{
    protected static $defaultName = 'analytics';

    public function getCommandDescription(): string
    {
        return 'Show analytics for exam';
    }

    protected function setTableBody(int $page, Table $table, Exam $exam): void
    {
        $analytics = new Analytics($exam);
        collect($exam->getQuestions())
            ->slice($page * self::MAX_PER_PAGE, self::MAX_PER_PAGE)
            ->each(static function (array $question, int $questionNumber) use ($exam, &$table, $analytics) {
                $table->addRow([
                    $question[0],
                    $analytics->getPValueForQuestion($questionNumber),
                    $analytics->getCorrelationScoreForQuestion($questionNumber),
                ]);
            });
    }

    protected function getHeaders(): array
    {
        return ['Question', 'pValue', 'Correlation'];
    }

    protected function getTotalNrOfPages(Exam $exam): float
    {
        return \count($exam->getQuestions()) / self::MAX_PER_PAGE;
    }
}
