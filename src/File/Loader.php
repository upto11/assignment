<?php

declare(strict_types=1);

namespace App\File;

use App\File\Handler\FileHandlerInterface;

class Loader
{
    /**
     * @var FileHandlerInterface
     */
    private $fileHandler;

    /**
     * FileLoader constructor.
     */
    public function __construct(FileHandlerInterface $fileHandler)
    {
        $this->fileHandler = $fileHandler;
    }

    /**
     * Load a file using the given file handler.
     */
    public function load(string $file): void
    {
        $this->fileHandler->load($file);

        //@TODO implement validation!
    }

    /**
     * Simple proxy to the given handler to get the data as an array.
     */
    public function getData(): array
    {
        return $this->fileHandler->getData();
    }
}
