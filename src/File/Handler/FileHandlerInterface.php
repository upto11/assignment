<?php

declare(strict_types=1);

namespace App\File\Handler;

interface FileHandlerInterface
{
    public function load(string $file): FileHandlerInterface;

    public function getData(): array;
}
