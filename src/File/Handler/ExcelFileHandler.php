<?php

declare(strict_types=1);

namespace App\File\Handler;

use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class ExcelFileHandler implements FileHandlerInterface
{
    /**
     * @var Spreadsheet
     */
    private $loader;

    /**
     * @var array
     */
    private $data = [];

    /**
     * ExcelFileHandler constructor.
     */
    public function __construct()
    {
        $this->loader = new Spreadsheet();
    }

    /**
     * @{@inheritdoc}
     *
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function load(string $file): FileHandlerInterface
    {
        $reader = new Xlsx();
        $data = $reader->load($file);

        $this->data = $data->getActiveSheet()->toArray();

        return $this;
    }

    public function getData(): array
    {
        return $this->data;
    }
}
