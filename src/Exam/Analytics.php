<?php

declare(strict_types=1);

namespace App\Exam;

use MathPHP\Statistics\Correlation;

class Analytics
{
    /**
     * @var Exam
     */
    private $exam;

    public function __construct(Exam $exam)
    {
        $this->exam = $exam;
    }

    public function getCorrelationScoreForQuestion(int $question): float
    {
        $scores = [];
        $grades = [];
        foreach ($this->exam->getStudents() as $student) {
            $scores[] = $student->getScores()[$question];
            $grades[] = $this->exam->getGrade($student);
        }

        $correlation = Correlation::r($scores, $grades);

        return round(is_nan($correlation) ? 0.0 : $correlation, 2);
    }

    /**
     * @param int $question
     */
    public function getPValueForQuestion($question): float
    {
        $currentQuestion = $this->exam->getQuestions()[$question];

        $averageScore = collect($this->exam->getStudents())
            ->average(static function (Student $student) use ($question): float {
                return $student->getScores()[$question];
            });

        return round($averageScore / $currentQuestion[1], 2);
    }
}
