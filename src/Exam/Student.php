<?php

declare(strict_types=1);

namespace App\Exam;

class Student
{
    protected $name;
    protected $scores = [];

    /**
     * Student constructor.
     */
    public function __construct(string $name, array $scores)
    {
        $this->name = $name;
        $this->scores = $scores;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getScores(): array
    {
        return $this->scores;
    }
}
