<?php

declare(strict_types=1);

namespace App\Exam;

/**
 * Abstracts data from a given array into a usable and understandable format.
 */
class ExamData
{
    /**
     * @var array
     */
    protected $questions = [];

    /**
     * @var array
     */
    protected $maxScores = [];

    /**
     * @var Student[]
     */
    protected array $students = [];

    /**
     * Try and parse data from an array into a reusable object.
     * This method will also verify the data structure.
     *
     * The structure we expect to see is:
     *  [
     *      [ 'ID', 'Score Question 1', 'Score Question 2' ... etc ],
     *      [ 'Max question score', 1, 4, 5 ... etc ],
     *      [ 'Student name', 1, 0, 0 ... etc ]
     *  ]
     *
     * Data will be normalized as in, for questions and max scores the
     * first column will be stripped.
     */
    public function __construct(array $data)
    {
        $this->maxScores = $data[1];
        array_shift($this->maxScores);

        $questions = $data[0];
        array_shift($questions);
        $this->normalizeQuestions($questions);

        $students = collect($data)->slice(2)->toArray();
        $this->normalizeStudents($students);
    }

    public function getQuestions(): array
    {
        return $this->questions;
    }

    public function getMaxScores(): array
    {
        return $this->maxScores;
    }

    public function getStudents(): array
    {
        return $this->students;
    }

    protected function normalizeQuestions(array $questions): void
    {
        $this->questions = collect($questions)
            ->zip($this->getMaxScores())
            ->toArray();
    }

    protected function normalizeStudents(array $students): void
    {
        collect($students)->each(function (array $item) {
            $this->students[] = new Student($item[0], collect($item)->slice(1)->values()->toArray());
        });
    }
}
