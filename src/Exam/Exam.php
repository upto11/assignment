<?php

declare(strict_types=1);

namespace App\Exam;

use App\Exam\Grading\DefaultGradingTemplate;
use App\Exam\Grading\GradingTemplateInterface;

/**
 * This represents a single exam taken by a number of students.
 */
class Exam
{
    /**
     * @var ExamData
     */
    protected ExamData $examData;

    /**
     * @var GradingTemplateInterface
     */
    protected GradingTemplateInterface $gradingTemplate;

    /**
     * Setup a single Exam using ExamData as input.
     */
    public function __construct(ExamData $data, GradingTemplateInterface $gradingTemplate = null)
    {
        $this->examData = $data;
        $this->gradingTemplate = $gradingTemplate ?? new DefaultGradingTemplate();
    }

    public function studentPassed(Student $student): bool
    {
        return $this->getScorePercentage($student) >= $this->gradingTemplate::passingPercentage();
    }

    public function getScorePercentage(Student $student): float
    {
        return round((array_sum($student->getScores()) / $this->getMaximumScore()) * 100, 2, PHP_ROUND_HALF_UP);
    }

    protected function getMaximumScore(): float
    {
        return array_sum($this->examData->getMaxScores());
    }

    /**
     * @return Student[]
     */
    public function getStudents(): array
    {
        return $this->examData->getStudents();
    }

    public function getQuestions(): array
    {
        return $this->examData->getQuestions();
    }

    /**
     * Calculate grade for given student. Rules for the calculation are:
     *      < 20 = grade is always 1.0
     *      < 70 = failed exam.
     *      >= 70 = passed exam.
     */
    public function getGrade(Student $student): float
    {
        $percentageScored = $this->getScorePercentage($student);

        if ($percentageScored < $this->gradingTemplate::failingPercentage()) {
            return $this->gradingTemplate::minGrade();
        }

        if ($percentageScored < $this->gradingTemplate::passingPercentage()) {
            return $this->calculateScore($percentageScored, $this->gradingTemplate::passingGrade(), $this->gradingTemplate::minGrade(), $this->gradingTemplate::passingPercentage(), $this->gradingTemplate::failingPercentage());
        }

        return $this->calculateScore($percentageScored, $this->gradingTemplate::maxGrade(), $this->gradingTemplate::passingGrade(), $this->gradingTemplate::maxPercentage(), $this->gradingTemplate::passingPercentage());
    }

    protected function calculateScore(float $percentageScored, float $fallbackGrade, float $minGrade, int $maxPercentage, int $minPercentage): float
    {
        $factor = $this->getGradeFactor($fallbackGrade, $minGrade, $maxPercentage, $minPercentage);

        return $minGrade + ($percentageScored - $minPercentage) * $factor;
    }

    protected function getGradeFactor(float $fallbackGrade, float $minGrade, int $maxPercentage, int $minPercentage): float
    {
        return ($fallbackGrade - $minGrade) / ($maxPercentage - $minPercentage);
    }
}
