<?php

declare(strict_types=1);

namespace App\Exam\Grading;

abstract class AbstractGradingTemplate implements GradingTemplateInterface
{
    public static function maxPercentage(): int
    {
        return 100;
    }

    public static function maxGrade(): float
    {
        return 10.0;
    }
}
