<?php

declare(strict_types=1);

namespace App\Exam\Grading;

interface GradingTemplateInterface
{
    public static function maxGrade(): float;

    public static function minGrade(): float;

    public static function passingGrade(): float;

    public static function passingPercentage(): int;

    public static function failingPercentage(): int;

    public static function maxPercentage(): int;
}
