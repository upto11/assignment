<?php

declare(strict_types=1);

namespace App\Exam\Grading;

/**
 * Default grading template used for most tests.
 * This template allows for students to:.
 *
 *      - Pass when they have a >=70% score
 *      - Get a 5.5 at 70%
 *      - Can't get lower then a 1.0 grade
 */
class DefaultGradingTemplate extends AbstractGradingTemplate
{
    public static function minGrade(): float
    {
        return 1.0;
    }

    public static function passingGrade(): float
    {
        return 5.5;
    }

    public static function passingPercentage(): int
    {
        return 70;
    }

    public static function failingPercentage(): int
    {
        return 20;
    }
}
