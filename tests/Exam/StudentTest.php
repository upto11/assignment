<?php

namespace App\Tests\Exam;

use App\Exam\Student;
use PHPUnit\Framework\TestCase;

class StudentTest extends TestCase
{
    /**
     * @dataProvider studentsData
     */
    public function testCanSetNameAndScores(string $name, array $scores): void
    {
        $student = new Student($name, $scores);

        $this->assertEquals($name, $student->getName());
        $this->assertEquals($scores, $student->getScores());
    }

    public function studentsData(): array
    {
        return [
            // name, scores, exam maximum score
            ['John Doe', [1, 1, 3, 2], 10],
            ['Maria Johnson', [1, 1, 1, 2], 10],
            ['Rufus Dufus', [0, 1, 0, 2], 20],
            ['Rockstar Joe', [1, 1, 1, 1], 4],
        ];
    }
}
