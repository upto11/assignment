<?php

namespace App\Tests\Exam;

use App\Exam\Exam;
use App\Exam\ExamData;
use PHPUnit\Framework\TestCase;

class ExamTest extends TestCase
{
    /**
     * @dataProvider studentsData
     */
    public function testGetGradeAccordingToThresholds(string $name, array $scores, float $expectedGrade)
    {
        $data = new ExamData([
            ['id', 'q1', 'q2', 'q3', 'q4'],
            ['max_score', 2, 3, 2, 3],
            [$name, ...$scores],
        ]);

        $exam = new Exam($data);

        $this->assertEquals($exam->getGrade($exam->getStudents()[0]), $expectedGrade);
    }

    /**
     * @dataProvider studentsData
     */
    public function testCanTellIfStudentHasPassedExam(string $name, array $scores, float $expectedGrade, bool $expectedPassed)
    {
        $data = new ExamData([
            ['id', 'q1', 'q2', 'q3', 'q4'],
            ['max_score', 2, 3, 2, 3],
            [$name, ...$scores],
        ]);

        $exam = new Exam($data);

        $this->assertEquals($exam->studentPassed($exam->getStudents()[0]), $expectedPassed);
    }

    public function studentsData(): array
    {
        return [
            // name, scores, expected percentage grade, passed exam
            ['John Doe', [2, 3, 2, 3], 10.0, true],
            ['Maria Johnson', [3, 3, 1, 0], 5.5, true],
            ['Rufus Dufus', [0, 3, 2, 3], 7.0, true],
            ['Rockstar Joe', [0, 0, 0, 0], 1.0, false],
            ['Rockstar Joe', [2, 3, 1, 0], 4.6, false],
        ];
    }
}
